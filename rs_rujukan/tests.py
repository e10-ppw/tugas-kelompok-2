from django.test import TestCase
from django.test import Client
from django.urls import resolve
from rs_rujukan.models import RumahSakit
from .views import index, search

class UnitTestRsRujukan(TestCase):
    def test_count_rs(self):
        rs = RumahSakit.objects.create(name='RS Pertamina', province='DKI Jakarta', address='Jl. Kalimalang', telp='500-505')
        rs.save()
        count_table = RumahSakit.objects.all().count()
        self.assertEqual(count_table, 1)

    def test_rs_models(self):
        target_rs_name = 'RS Pertamina'
        rs = RumahSakit.objects.create(name=target_rs_name, province='DKI Jakarta', address='Jl. Kalimalang', telp='500-505')
        target_rs = RumahSakit.objects.get(name='RS Pertamina')
        self.assertEqual(str(target_rs), target_rs_name)

    def test_rs_views(self):
        target = resolve('/rs-rujukan/')
        self.assertEqual(target.func, index)

    def test_rs_use_template(self):
        response = Client().get('/rs-rujukan/')
        self.assertTemplateUsed(response, 'rs/rs-rujukan.html')

    def test_rs_page_exist(self):
        response = Client().get('/rs-rujukan/')
        self.assertEqual(response.status_code, 200)

    def test_rs_get_content(self):
        response = Client().get('/rs-rujukan/', follow=True)
        content = response.content.decode('utf8')
        self.assertIn('RS Rujukan', content)