from django import forms
from .models import KritikSaran

class FormKritik(forms.ModelForm):
    class Meta:
        model = KritikSaran
        fields = ['nama', 'email', 'kritik_saran']

        labels = {
            'nama' : 'Nama Anda',
            'email' : 'E-mail Anda',
            'kritik_saran' : 'Pesan Anda'
        }

        widgets = {
            'nama' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'type' : 'text',
                    'placeholder' : 'Maks 100 karakter'
                }
            ),
            'email' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'type' : 'email',
                    'placeholder' : 'Ex: coformant@gmail.com'
                }
            ),
            'kritik_saran' : forms.Textarea(
                attrs = {
                    'class' : 'form-control',
                    'type' : 'text',
                    'placeholder' : 'Maks 1.000 karakter'
                }
            )
        }