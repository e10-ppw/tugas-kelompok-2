from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import FormKritik
from .models import KritikSaran
from django.http import HttpResponse, HttpResponseRedirect

def home(request):
    form = FormKritik(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        form.save()
        messages.success(request, 'Tanggapan Anda telah kami simpan')
        return redirect('beranda:home')
        
    context = {
        'input_form' : form
    }
    return render(request, 'beranda/home.html', context)

def daftar(request):
    kritikans = KritikSaran.objects.all()
    context = {
        'daftar_kritik' : kritikans
    }
    
    return render(request, 'beranda/kritik-saran.html', context)