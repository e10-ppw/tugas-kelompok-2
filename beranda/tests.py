from django.test import TestCase, Client
from django.urls import resolve

from .models import KritikSaran
from .views import *
from .urls import *

class BerandaTestCase(TestCase):

# URL Test
    def test_url_home_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_daftar_is_exist(self):
        response = Client().get('/daftar-kritikan/')
        self.assertEqual(response.status_code, 200)

# Template Used Test
    def test_using_home_template(self):
        response = Client().get('/')            
        self.assertTemplateUsed(response, 'beranda/home.html')

    def test_using_daftar_kritikan_template(self):
        response = Client().get('/daftar-kritikan/')            
        self.assertTemplateUsed(response, 'beranda/kritik-saran.html')

# Views Test
    def test_views_home(self):
        found = resolve('/')            
        self.assertEqual(found.func, home)

    def test_views_daftar(self):
        found = resolve('/daftar-kritikan/')            
        self.assertEqual(found.func, daftar)

# Module Test
    def test_model_create_kritik(self):
        KritikSaran.objects.create(nama="Lela", email="test@gmail.com", kritik_saran="ini adalah test")
        jumlah = KritikSaran.objects.all().count()
        self.assertEqual(jumlah, 1)