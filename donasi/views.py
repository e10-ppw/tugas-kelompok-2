from django.shortcuts import render, redirect
from django import forms
from django.http import HttpResponse, HttpResponseRedirect
from donasi.models import fitur_donasi
from .forms import DonasiForm
from .models import fitur_donasi

# Create your views here.
def form_donasi(request):
    data_donasi = fitur_donasi.objects.all()
    form = DonasiForm(request.POST or None)
    donate_context = {
        'data_donasi' : data_donasi,
        'form' : form,
    }
    if request.method == "POST":
        if form.is_valid():
            data_donasi.create(
                nama_donasi = form.cleaned_data.get('nama_donasi'),
                nama_pemberi = form.cleaned_data.get('nama_pemberi'),
                url = form.cleaned_data.get('url'),
            )
            return redirect('/donasi')

    return render(request, 'formdonasi.html', donate_context)

def donasi(request):
    tambah_link = DonasiForm()
    data = fitur_donasi.objects.all()
    response = {
        'tambah_link': tambah_link,
        'data': data,
    }
    return render(request, 'donasi.html', response)