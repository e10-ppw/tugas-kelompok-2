from django import forms

from donasi.models import fitur_donasi

class DonasiForm(forms.Form):
    nama_donasi = forms.CharField(label='Nama Donasi :', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ex: Donasi RS Harapan Ibu'}), required=True)

    nama_pemberi = forms.CharField(label='Author :', max_length=50,
                                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'ex: Nama anda/institusi/organisasi/dll'}), required=True)

    url = forms.CharField(label='URL Donasi :', max_length=50,
                                    widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'masukkan URL'}), required=True)