from django.db import models

# Create your models here.
class fitur_donasi(models.Model):
    nama_donasi = models.CharField(max_length=100)
    nama_pemberi = models.CharField(max_length=100)
    url = models.CharField(max_length=100)

    def __str__(self):
        return self.nama_donasi