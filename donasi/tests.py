from django.test import TestCase
from django.test import Client
from django.urls import resolve
from donasi.models import fitur_donasi
from .views import donasi, form_donasi

# Create your tests here.
class UnitTestDonasi(TestCase):
    def test_main_page(self):
        response = Client().get('/donasi/')
        self.assertEqual(response.status_code, 200)
    
    def test_template_donasi(self):
        response = Client().get('/donasi/')
        self.assertTemplateUsed(response, 'donasi.html')

    def test_template_formdonasi(self):
        response = Client().get('/donasi/formdonasi/')
        self.assertTemplateUsed(response, 'formdonasi.html')

    def test_url_fungsi(self): 
    	found = resolve('/donasi/')
    	self.assertEqual(found.func, donasi)

    def test_url_fungsi2(self): 
    	found = resolve('/donasi/formdonasi/')
    	self.assertEqual(found.func, form_donasi)

    def test_text_on_page1(self):
        response = Client().get('/donasi/')
        content = response.content.decode('utf8')
        self.assertIn("DONASI", content)        
        self.assertIn("Ingin menambahkan donasi?", content)
    
    def test_text_on_page2(self):
        response = Client().get('/donasi/formdonasi/')
        content = response.content.decode('utf8')
        self.assertIn("Tambahkan Donasi", content)
    
    def test_donasi_model_kegiatan(self):
        fitur_donasi.objects.create(nama_donasi='peduli')
        donasii = fitur_donasi.objects.get(nama_donasi='peduli')
        self.assertEqual(str(donasii), 'peduli')