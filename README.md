# Tugas Kelompok 1 - E10

[![pipeline status](https://gitlab.com/e10-ppw/tugas-kelompok-1/badges/master/pipeline.svg)](https://gitlab.com/e10-ppw/tugas-kelompok-1/-/commits/master)

[![coverage report](https://gitlab.com/e10-ppw/tugas-kelompok-1/badges/master/coverage.svg)](https://gitlab.com/e10-ppw/tugas-kelompok-1/-/commits/master)


## Nama Anggota

- Tri Rahayu (1706984770)
- Annisa Amalia S (1906354002)
- Sefrina Fauranisa (1906398944)
- Lela Monica Fitriany (1906353896)

## Link Heroku

http://coformant.herokuapp.com/

## Link Persona

https://docs.google.com/document/d/17oLVetu4czizU3YjKnFgwnwjLUXr8LZsaK6g5FVrfn8/edit

## Deskripsi Aplikasi

Dewasa ini, banyak sekali informasi-informasi mengenai COVID-19 yang bertebaran di internet. Hal ini menyebabkan masyarakat terkadang bingung harus mengambil informasi dari mana. Karena itu, kami berencana membuat aplikasi penyedia dan pengumpul informasi-informasi tersebut. Aplikasi ini terdiri dari beberapa fitur yang diantaranya akan men-direct ke web asli dari suatu informasi (seperti web resmi COVID yang dimiliki pemerintah). Dengan adanya aplikasi ini, kami berharap masyarakat tidak kebingungan dalam mencari informasi.

## Fitur Aplikasi

1. "Beranda", berisikan infografis mengenai jumlah-jumlah orang positif COVID-19 di Indonesia.

2. "Donasi", berisikan daftar crowdfunding yang tersedia.

3. "RS Rujukan", berisikan daftar rumah sakit rujukan COVID-19 di Indonesia.

4. "Daftar Situs", berisikan daftar situs resmi penganggulangan COVID-19 dari pemerintah di berbagai macam provinsi di Indonesia.
